
import spark.ModelAndView;
import spark.template.thymeleaf.ThymeleafTemplateEngine;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;

public class HelloWorld {

    //default port 4567
    public static void main(final String[] args) {

        get("/", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("name", "Fellow");
            return new ModelAndView(model, "hello");
        }, new ThymeleafTemplateEngine());

    }
}
